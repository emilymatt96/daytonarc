#!/bin/bash

set -e

apt update
apt install -y --no-install-recommends \
  build-essential \
  cmake \
  gcc \
  gdb \
  g++ \
  libcurl4-openssl-dev \
  libcurlpp-dev \
  libgpiod-dev \
  libspdlog-dev \
  make \
  ninja-build \
  pkg-config \
  rapidjson-dev

apt autoremove -y
apt upgrade -y
apt clean
