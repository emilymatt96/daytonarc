cmake_minimum_required(VERSION 3.10)
project(Daytona)
set(CMAKE_CXX_STANDARD 17)

option(BUILD_TESTS OFF)

add_definitions(-DFMT_HEADER_ONLY)
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -Wextra -Werror -pedantic -pthread -pipe -fPIC -fPIE")
if (${CMAKE_BUILD_TYPE} MATCHES Debug)
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -g -ggdb3 -O0 -fsanitize=address -fno-omit-frame-pointer")
    if (${BUILD_TESTS} MATCHES ON)
        set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} --coverage")
    endif ()
else ()
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -O3 -s  -flto")
    add_definitions(-DFORTIFY_SOURCE=2)
endif ()

find_package(PkgConfig REQUIRED)
pkg_check_modules(RAPIDJSON RapidJSON REQUIRED)
pkg_check_modules(SPDLOG spdlog REQUIRED)
pkg_check_modules(CURLPP curlpp REQUIRED)
pkg_check_modules(GPIOD libgpiodcxx REQUIRED)

include_directories(src)

add_library(daytona_lib STATIC
        src/gpio/GPIOController.cpp
        src/gpio/GPIOPin.cpp
        src/gpio/PWMPin.cpp
        src/logging/Logging.cpp
        src/utils/EnvUtils.cpp
        src/utils/SystemUtils.cpp
        )

add_executable(daytona src/main.cpp)

target_link_libraries(daytona
        -Wl,--as-needed,--start-group
        daytona_lib
        ${CURLPP_LIBRARIES}
        ${GPIOD_LIBRARIES}
        -Wl,--end-group
        )
