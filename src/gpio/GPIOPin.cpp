//
// Created by emily on 12/4/22.
//

#include "GPIOPin.h"
#include "logging/Logging.h"

#include <spdlog/logger.h>

namespace Daytona::GPIO {
    GPIOPin::GPIOPin(unsigned int offset, gpiod::line line, GPIODirection direction) :
            m_logger(Logging::getLogger("GPIOPin " + std::to_string(offset))),
            m_line(std::move(line)),
            m_direction(direction) {
        m_direction == GPIODirection::OUTPUT ? m_line.set_direction_output() : m_line.set_direction_input();
        m_logger->info("Created controller for GPIO Pin {}", offset);
    }

    GPIOPin::~GPIOPin() {
        writeValue(false);
    }

    GPIODirection GPIOPin::getDirection() {
        return m_direction;
    }

    bool GPIOPin::readValue() {
        try {
            if (m_direction != GPIODirection::INPUT) {
                throw std::runtime_error("Attempting to read GPIO pin value, but pin is set to OUTPUT");
            }
            return m_line.get_value();
        } catch (std::system_error &err) {
            m_logger->error(err.what());
            return false;
        }
    }

    void GPIOPin::writeValue(bool value) {
        try {
            if (m_direction != GPIODirection::OUTPUT) {
                throw std::runtime_error("Attempting to write GPIO pin value, but pin is set to INPUT");
            }
            m_line.set_value(value);
        } catch (std::system_error &err) {
            m_logger->error(err.what());
        }
    }
}