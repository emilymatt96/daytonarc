//
// Created by emily on 12/3/22.
//

#include "GPIOController.h"

#include <spdlog/logger.h>
#include <gpiod.hpp>
#include <utility>

#include "logging/Logging.h"

#include "GPIOPin.h"
#include "PWMPin.h"

namespace Daytona::GPIO {
    std::shared_ptr<GPIOController> GPIOController::createInstance(const std::string &device_name) {
        return std::shared_ptr<GPIOController>(new GPIOController(device_name));
    }

    GPIOController::GPIOController(std::string device_name) : m_logger(Logging::getLogger("GPIOController")),
                                                              m_device_name(std::move(device_name)),
                                                              m_device(nullptr) {
    }

    GPIOController::~GPIOController() {
        m_logger->info("Cleaning up GPIO Controller");
    }

    bool GPIOController::init() {
        try {
            m_device = std::make_unique<gpiod::chip>(m_device_name);
        } catch (std::exception &e) {
            m_logger->error(e.what());
            return false;
        }
        return true;
    }

    GPIOEitherType GPIOController::getPin(unsigned int offset) {
        auto used_pin = m_used_gpio_map.find(offset);
        if (used_pin == m_used_gpio_map.end()) {
            return {GPIOType::NONE, {}};
        }

        return used_pin->second;
    }

    template<>
    std::shared_ptr<GPIOPin> GPIOController::createPin(unsigned int offset, GPIODirection direction) {
        auto used_pin = m_used_gpio_map.find(offset);
        if (used_pin != m_used_gpio_map.end()) {
            m_logger->error("Pin is already in use!");
            return nullptr;
        }

        try {
            auto pin_line = m_device->get_line(offset);
            pin_line.request(
                    {m_logger->name(),
                     GPIOD_LINE_REQUEST_DIRECTION_AS_IS,
                     0});
            auto new_pin = std::make_shared<GPIOPin>(offset, pin_line, direction);
            m_used_gpio_map[offset] = {GPIOType::GPIO, new_pin};
            return new_pin;
        } catch (std::exception &err) {
            m_logger->error(err.what());
            return nullptr;
        }
    }

    template<>
    std::shared_ptr<PWMPin> GPIOController::createPin(unsigned int offset, GPIODirection direction) {
        auto used_pin = m_used_gpio_map.find(offset);
        if (used_pin != m_used_gpio_map.end()) {
            m_logger->error("Pin is already in use!");
            return nullptr;
        }

        try {
            auto pin_line = m_device->get_line(offset);
            pin_line.request(
                    {m_logger->name(),
                     GPIOD_LINE_REQUEST_DIRECTION_AS_IS,
                     0});
            auto new_pin = std::make_shared<PWMPin>(offset, pin_line, direction);
            m_used_gpio_map[offset] = {GPIOType::PWM, new_pin};
            return new_pin;
        } catch (std::exception &err) {
            m_logger->error(err.what());
            return nullptr;
        }
    }

    bool GPIOController::dropPin(unsigned int offset) {
        auto pin_used = m_used_gpio_map.find(offset);
        if (pin_used != m_used_gpio_map.end()) {
            m_used_gpio_map.erase(pin_used);
            return true;
        }

        m_logger->warn("Pin {} not found, perhaps it is not active?", offset);
        return false;
    }
}