//
// Created by emily on 12/4/22.
//

#include "PWMPin.h"
#include "logging/Logging.h"

#include <gpiod.hpp>
#include <spdlog/logger.h>

namespace Daytona::GPIO {
    PWMPin::PWMPin(unsigned int offset, gpiod::line line, GPIODirection direction) :
            m_logger(Logging::getLogger("PWMPin" + std::to_string(offset))),
            m_line(std::move(line)),
            m_direction(direction),
            m_enabled(false),
            m_period_ns(1000),
            m_duty_cycle(0.0f) {
        m_direction == GPIODirection::OUTPUT ? m_line.set_direction_output() : m_line.set_direction_input();
        m_logger->info("Created GPIO controller for PWM pin {}", offset);
    }

    PWMPin::~PWMPin() {
        disable();
    }

    void PWMPin::inputDuty() {
        auto currentValue = 0.0f;
        while (m_enabled) {
            try {
                currentValue += static_cast<float>(m_line.get_value());
                currentValue /= 2;

                m_duty_cycle = currentValue;
            } catch (std::system_error &err) {
                m_logger->error(err.what());
            }
            std::this_thread::sleep_for(std::chrono::milliseconds(1));
        }
    }

    void PWMPin::outputDuty() {
        // In order to prevent memory access to the heap as much as possible
        bool current_value = false;
        auto time_to_wait = static_cast<unsigned int>(std::floor(static_cast<float>(m_period_ns) * m_duty_cycle));
        while (m_enabled) {
            try {
                m_line.set_value(current_value);
                current_value = !current_value;
            } catch (std::system_error &err) {
                m_logger->error(err.what());
            }
            std::this_thread::sleep_for(std::chrono::nanoseconds(time_to_wait));
        }
        m_line.set_value(0);
    }

    void PWMPin::enable() {
        m_enabled = true;
        m_pwm_thread = std::thread([&] {
            m_direction == GPIODirection::OUTPUT ? outputDuty() : inputDuty();
        });
    }

    void PWMPin::disable() {
        m_enabled = false;
        if (m_pwm_thread.joinable()) {
            m_pwm_thread.join();
        }
    }

    void PWMPin::setPeriodNanos(unsigned int period_nanos) {
        if (m_direction == GPIODirection::INPUT) {
            throw std::runtime_error(fmt::format("Attempting to set run period for a PWM pin in INPUT mode"));
        }

        // Attempt to maintain current state
        bool is_enabled = m_enabled;
        disable();

        m_period_ns = period_nanos;

        // restore state
        if (is_enabled) {
            enable();
        }
    }

    void PWMPin::setPeriodMicros(unsigned int period_micros) {
        setPeriodNanos(period_micros * 1000);
    }

    void PWMPin::setPeriodMillis(unsigned int period_millis) {
        setPeriodMicros(period_millis * 1000);
    }


    void PWMPin::setDutyCycle(float duty_cycle) {
        if (m_direction == GPIODirection::INPUT) {
            throw std::runtime_error(fmt::format("Attempting to set duty cycle for a PWM pin in INPUT mode"));
        }

        if (duty_cycle <= 0.f || duty_cycle >= 1.f) {
            throw std::runtime_error(fmt::format("Impossible value received {}, value should be between 0.0f and 1.0f",
                                                 duty_cycle));
        }

        // Attempt to maintain current state
        bool is_enabled = m_enabled;
        disable();

        m_duty_cycle = duty_cycle;

        // Restore state
        if (is_enabled) {
            enable();
        }
    }

    float PWMPin::getDutyCycle() {
        return m_duty_cycle;
    }
}