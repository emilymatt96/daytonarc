//
// Created by emily on 12/4/22.
//

#pragma once

#include <thread>
#include <memory>
#include <atomic>
#include <gpiod.hpp>

#include "enums/GPIOEnums.h"

namespace spdlog {
    class logger;
}
namespace Daytona::GPIO {
    class PWMPin {
    public:
        PWMPin(unsigned int offset, gpiod::line line, GPIODirection direction);

        ~PWMPin();

        void enable();

        void disable();

        void setPeriodMillis(unsigned int period_millis);

        void setPeriodMicros(unsigned int period_micros);

        void setPeriodNanos(unsigned int period_nanos);

        void setDutyCycle(float duty_cycle);

        float getDutyCycle();

        void outputDuty();

        void inputDuty();

    private:
        std::shared_ptr<spdlog::logger> m_logger;

        gpiod::line m_line;
        GPIODirection m_direction;

        std::atomic<bool> m_enabled;
        std::thread m_pwm_thread;

        unsigned int m_period_ns;
        std::atomic<float> m_duty_cycle;
    };
}
