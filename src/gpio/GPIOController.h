//
// Created by emily on 12/3/22.
//

#pragma once

#include <memory>
#include <string>
#include <map>
#include <variant>
#include <stdexcept>
#include "enums/GPIOEnums.h"

namespace spdlog {
    class logger;
}


namespace gpiod {
    class chip;
}

namespace Daytona::GPIO {
    class GPIOPin;

    class PWMPin;

    typedef std::pair<GPIOType, std::variant<std::shared_ptr<GPIOPin>, std::shared_ptr<PWMPin>>> GPIOEitherType;

    class GPIOController {
    public:
        static std::shared_ptr<GPIOController> createInstance(const std::string &device_name);

        ~GPIOController();

        bool init();

        GPIOEitherType getPin(unsigned int offset);

        template<typename T>
        std::shared_ptr<T> createPin(unsigned int offset, GPIODirection direction);

        bool dropPin(unsigned int offset);

    private:
        explicit GPIOController(std::string device_name);

        std::shared_ptr<spdlog::logger> m_logger;
        std::string m_device_name;
        std::unique_ptr<gpiod::chip> m_device;

        std::map<unsigned int, GPIOEitherType> m_used_gpio_map;
    };
}


template<>
std::shared_ptr<Daytona::GPIO::GPIOPin>
Daytona::GPIO::GPIOController::createPin(unsigned int offset, GPIODirection direction);

template<>
std::shared_ptr<Daytona::GPIO::PWMPin>
Daytona::GPIO::GPIOController::createPin(unsigned int offset, GPIODirection direction);
