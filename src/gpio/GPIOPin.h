//
// Created by emily on 12/4/22.
//

#pragma once

#include <memory>
#include <gpiod.hpp>
#include "enums/GPIOEnums.h"

namespace spdlog {
    class logger;
}

namespace Daytona::GPIO {
    class GPIOPin {
    public:
        GPIOPin(unsigned int offset, gpiod::line line, GPIODirection direction);

        ~GPIOPin();

        GPIODirection getDirection();

        void writeValue(bool value);

        bool readValue();

    private:
        std::shared_ptr<spdlog::logger> m_logger;
        gpiod::line m_line;
        GPIODirection m_direction;
    };
}
