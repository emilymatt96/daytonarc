//
// Created by emily on 10/18/22.
//

#include "Logging.h"

#include <spdlog/spdlog.h>
#include <spdlog/sinks/rotating_file_sink.h>
#include <spdlog/sinks/syslog_sink.h>
#include <iostream>
#include <filesystem>

#include "utils/EnvUtils.h"

namespace Daytona::Logging {
// To ensure the entire program's getting and deleting of loggers is ok
    static std::mutex kLoggersMutex;
    static std::vector<spdlog::sink_ptr> kSinks;

    spdlog::sink_ptr getFileSink(const std::string &fileEnv) {
        std::cout << "Will attempt writing logs to file " << fileEnv << std::endl;

        unsigned int logFileMax = 1024 * 1024 * 5;
        auto fileSizeEnv = std::getenv("LOG_FILE_SIZE");
        if (fileSizeEnv) {
            try {
                std::stoul(fileSizeEnv);
            } catch (...) {
                std::cerr << "Could not parse value of " << fileSizeEnv << ", Will use default of 5MB" << std::endl;
            }
        }
        std::shared_ptr<spdlog::sinks::rotating_file_sink_mt> fileSink = nullptr;
        try {
            fileSink = std::make_shared<spdlog::sinks::rotating_file_sink_mt>(fileEnv, logFileMax, 5);
        } catch (...) {
            std::cerr << "Could not create file at " << fileEnv << ", will attempt at local folder" << std::endl;
            try {
                fileSink = std::make_shared<spdlog::sinks::rotating_file_sink_mt>(
                        fmt::format("./{}", std::filesystem::path(fileEnv).filename().string()), logFileMax, 5);
            } catch (...) {
                std::cerr << "Could not create log file, using stdout instead" << std::endl;
            }
        }
        return fileSink;
    }

    void setupSinks() {
        kSinks.emplace_back(std::make_shared<spdlog::sinks::ansicolor_stdout_sink_mt>());
        auto fileEnv = Daytona::EnvUtils::getEnvVariable<std::string>("LOG_FILE", "");
        if (!fileEnv.empty()) {
            auto fileSink = getFileSink(fileEnv);
            if (fileSink) {
                kSinks.emplace_back(fileSink);
            }
        }

        if (Daytona::EnvUtils::getEnvVariable<bool>("LOG_TO_SYSLOG", false)) {
            kSinks.emplace_back(std::make_shared<spdlog::sinks::syslog_sink_mt>("Daytona", LOG_PID, LOG_USER, true));
        }
    }

    void setupDebugLevel(const std::string &envName) {
        auto lvl = Daytona::EnvUtils::getEnvVariable<unsigned int>(envName.c_str(), 2);

        // Using specific casts here to prevent any ints coming about causing trouble
        auto logLevelInEnum = static_cast<spdlog::level::level_enum>(
                std::max(std::min(lvl, static_cast<unsigned int>(6)), static_cast<unsigned int>(0)));
        spdlog::info("Debug level: {}",
                     spdlog::level::to_string_view(logLevelInEnum));
        spdlog::set_level(logLevelInEnum);
    }

    std::shared_ptr<spdlog::logger> getLogger(const std::string &logger_name) {
        std::lock_guard<std::mutex> loggersLock(kLoggersMutex);
        auto logger = spdlog::get(logger_name);
        if (!logger) {
            logger = std::make_shared<spdlog::logger>(logger_name, kSinks.begin(), kSinks.end());
            spdlog::register_logger(logger);
        }
        return logger;
    }

    void dropLogger(const std::shared_ptr<spdlog::logger> &logger) {
        std::lock_guard<std::mutex> loggersLock(kLoggersMutex);
        spdlog::drop(logger->name());
    }

    void dropAllLoggers() {
        std::lock_guard<std::mutex> loggersLock(kLoggersMutex);
        spdlog::drop_all();
    }
} // namespace openptz
