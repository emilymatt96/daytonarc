//
// Created by emily on 10/18/22.
//

#pragma once

#include <memory>
#include <string>

namespace spdlog {
    class logger;
}

namespace Daytona::Logging {
    void setupDebugLevel(const std::string &envName);

    void setupSinks();

    std::shared_ptr<spdlog::logger> getLogger(const std::string &logger_name);

    void dropLogger(const std::shared_ptr<spdlog::logger> &logger);

    void dropAllLoggers();
} // namespace openptz
