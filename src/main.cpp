#include <spdlog/logger.h>

#include "logging/Logging.h"
#include "gpio/GPIOController.h"
#include "gpio/PWMPin.h"

#include <random>

int main() {
    Daytona::Logging::setupDebugLevel("DAYTONA_LOG");
    Daytona::Logging::setupSinks();

    auto app_logger = Daytona::Logging::getLogger("Daytona");
    app_logger->info("Starting Application");

    if (getuid() != 0) {
        app_logger->critical("This program requires GPIO access, please run as root or sudo");
        return EXIT_FAILURE;
    }

    auto gpioController = Daytona::GPIO::GPIOController::createInstance("/dev/gpiochip0");
    if (!gpioController->init()) {
        return EXIT_FAILURE;
    }

    auto pin = gpioController->createPin<Daytona::GPIO::PWMPin>(6, GPIODirection::OUTPUT);
    pin->setPeriodNanos(500 * 1000 * 1000);
    pin->enable();
    std::random_device dev;
    std::uniform_real_distribution<float> rng(0, 1);
    for (unsigned int i = 0; i < 1000; i++) {
        pin->setDutyCycle(rng(dev));
        std::this_thread::sleep_for(std::chrono::milliseconds(1000));
    }

    return EXIT_SUCCESS;
}
