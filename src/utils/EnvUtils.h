//
// Created by emily on 10/11/22.
//

#pragma once

// This is a really heavy file for the compiler, make sure you use it only once in a while, on relatively
// static code sections

#include <string>
#include <limits>
#include <stdexcept>
#include <iostream>

namespace Daytona::EnvUtils {
    template<typename T>
    T parse(const std::string &env_val, T default_value);

    template<>
    std::string parse(const std::string &env_val, std::string default_value);

    template<>
    bool parse(const std::string &env_val, bool default_value);

    template<>
    double parse(const std::string &env_val, double);

    template<>
    float parse(const std::string &env_val, float);

    template<>
    int parse(const std::string &env_val, int);

    template<>
    long parse(const std::string &env_val, long);

    template<>
    unsigned int parse(const std::string &env_val, unsigned int default_value);

    template<typename T>
    T getEnvVariable(const char *env_name, T default_value) {
        auto env_val = std::getenv(env_name);
        if (!env_val) return default_value;

        try {
            return parse<T>(env_val, default_value);
        } catch (std::exception &err) {
            std::cerr << err.what() << std::endl;
            return default_value;
        }
    }
}