//
// Created by emily on 11/4/22.
//

#include <fmt/format.h>
#include "EnvUtils.h"

namespace Daytona::EnvUtils {

    template<>
    std::string
    parse(const std::string &env_val, std::string default_value) { // NOLINT(performance-unnecessary-value-param)
        return env_val.empty() ? default_value : env_val;
    }

    template<>
    bool parse(const std::string &env_val, bool default_value) {
        return default_value ?
               !(env_val == "0" || env_val == "OFF" || env_val == "FALSE")
                             : (env_val == "1" || env_val == "ON" || env_val == "TRUE");
    }

    template<>
    double parse(const std::string &env_val, double /*default_value*/) {
        return std::stod(env_val);
    }

    template<>
    float parse(const std::string &env_val, float /*default_value*/) {
        return std::stof(env_val);
    }

    template<>
    int parse(const std::string &env_val, int /*default_value*/) {
        return std::stoi(env_val);
    }

    template<>
    long parse(const std::string &env_val, long /*default_value*/) {
        return std::stol(env_val);
    }

    template<>
    unsigned int parse(const std::string &env_val, unsigned int default_value) {
        long temp_val = parse(env_val, static_cast<long>(default_value));
        auto max_uint = std::numeric_limits<unsigned int>::max();
        if (temp_val < 0 || temp_val > static_cast<long>(max_uint)) {
            std::string err =
                    std::to_string(temp_val) + " is out of range for type unsigned int, returning default value";
            throw std::out_of_range(
                    fmt::format("{} is out of range for type unsigned int, returning default value of {}",
                                temp_val, default_value));
        }
        return temp_val;
    }
}