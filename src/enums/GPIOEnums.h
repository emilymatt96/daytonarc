//
// Created by emily on 12/4/22.
//

#pragma once

enum GPIODirection {
    INPUT,
    OUTPUT
};

enum GPIOType {
    NONE,
    GPIO,
    PWM
};